const LINK_CONTEXT_MENU_ID = "CUSTOM_CONTEXT_MENU_" + Date.now() + "link";
const QUOTE_CONTEXT_MENU_ID = "CUSTOM_CONTEXT_MENU_" + Date.now() + "quote";
const IMAGE_CONTEXT_MENU_ID = "CUSTOM_CONTEXT_MENU_" + Date.now() + "image";

var console = chrome.extension.getBackgroundPage().console;

chrome.extension.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.from == "mouseup") {
    console.log(msg.point);
    gPos = msg.point;
  }
  sendResponse({ response: "this shit" });
});

function sendLinkToServer(url) {
  (async () => {
    const response = await fetch("http://127.0.0.1:8088/addlink", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ link: url }),
    })
      .then((res) => {
        console.log("success");
        return res;
      })
      .catch((err) => {
        console.log("error");
      });
  })();
}

function sendQuoteToServer(url, title, quote) {
  (async () => {
    const response = await fetch("http://127.0.0.1:8088/addquote", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        context_link: url,
        context_title: title,
        quote: quote,
      }),
    });
  })();
  // console.log("sent.")
  // console.log(url+' '+title+' '+quote)
  // console.log(JSON.stringify({ 'context_link': url, 'context_title': title, 'quote': quote })
}

function sendImageLinkToServer(url) {
  (async () => {
    const response = await fetch("http://127.0.0.1:8088/addimage", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ link: url }),
    })
      .then((res) => {
        console.log("success");
        return res;
      })
      .catch((err) => {
        console.log("error");
      });
  })();
}

/* GRAB LINK */
function snagLinkHandler(selectedLink, tab) {
  if (selectedLink.menuItemId !== LINK_CONTEXT_MENU_ID) {
    return;
  }
  sendLinkToServer(selectedLink.linkUrl);
}

chrome.contextMenus.removeAll(function () {
  chrome.contextMenus.create({
    title: "SlogIt",
    contexts: ["link"],
    id: LINK_CONTEXT_MENU_ID,
  });
});
chrome.contextMenus.onClicked.addListener(snagLinkHandler);

/* GRAB QUOTE */
function snagQuoteHandler(selectedQuote, tab) {
  if (selectedQuote.menuItemId !== QUOTE_CONTEXT_MENU_ID) {
    return;
  }
  sendQuoteToServer(tab.url, tab.title, selectedQuote.selectionText);
}

chrome.contextMenus.removeAll(function () {
  chrome.contextMenus.create({
    title: "SlogIt",
    type: "normal",
    contexts: ["selection"],
    id: QUOTE_CONTEXT_MENU_ID,
  });
});
chrome.contextMenus.onClicked.addListener(snagQuoteHandler);

/* GRAB IMAGE */
function snagImageHandler(selectedLink, tab) {
  if (selectedLink.menuItemId !== IMAGE_CONTEXT_MENU_ID) {
    return;
  }
  console.log(selectedLink.srcUrl);
  sendImageLinkToServer(selectedLink.srcUrl);
}

chrome.contextMenus.removeAll(function () {
  chrome.contextMenus.create({
    title: "SlogItImage",
    contexts: ["image"],
    id: IMAGE_CONTEXT_MENU_ID,
  });
});
chrome.contextMenus.onClicked.addListener(snagImageHandler);

/* Extension itself */
chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
    let url = tabs[0].url;
    sendLinkToServer(url);
  });
});

chrome.commands.onCommand.addListener(function (command) {
  console.log("Command");
  console.log(command);
  if (command === "snagLink") {
    console.log("snaglink");
  }
});
