import React from "react";
import { useDispatch, useSelector } from "react-redux";

import LinkForLinks from "../images/link.png";
import LinkForQuotes from "../images/quote.png";
import LinkForImages from "../images/image.png";
import { Menu, Layout } from "antd";

import "antd/dist/antd.css";
import "/css/custom.css";
import { pageChange } from "../actions";

const { Sider } = Layout;

function Sidebar() {
  return (
    <button onClick={() => useDispatch(pageChange('links'))}>
      Here
    </button>
  )
}
// const Sidebar = () => {
//   console.log(pageChange)
//   const page = useSelector((state) => state.page);
//   useDispatch(pageChange('temp'))
//   console.log('oi', page)
//   return (
//     <Sider
//       width={80}
//       style={{
//         overflow: "auto",
//         height: "100vh",
//         position: "fixed",
//         left: 0,
//       }}
//     >
//       <Menu theme="dark" mode="inline">
//         <Menu.Item className="sideBarLink"></Menu.Item>

//         <Menu.Item
//           className="sideBarLink"
//           onClick={() => useDispatch(pageChange('links'))}
//         >
//           <img width={40} src={LinkForLinks} />
//         </Menu.Item>

//         <Menu.Item className="sideBarLink">
//           <img width={40} src={LinkForQuotes} />
//         </Menu.Item>

//         <Menu.Item className="sideBarLink">
//           <img width={40} src={LinkForImages} />
//         </Menu.Item>
//       </Menu>
//     </Sider>
//   );
// };

export default Sidebar;
