import React from "react"
import ReactDOM from "react-dom"
import{ SearchBar, Button, WhiteSpace, WingBlank } from 'antd'

class SearchBarExtend extends React.Component {
  state = {
    value: "",
  };
  componentDidMount() {
    this.autoFocusInst.focus();
  }
  onChange = (value) => {
    this.setState({ value });
  };
  clear = () => {
    this.setState({ value: "" });
  };
  handleClick = () => {
    this.manualFocusInst.focus();
  };
  render() {
    return (
      <div>
          Test
     </div>
    );
  }
}

export default SearchBarExtend