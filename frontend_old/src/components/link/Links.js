import React, { useEffect, useState } from "react";
import { FetchLinks } from "./FetchLinks.js";
// import { DeleteLink } from "./DeleteLink.js";
import { Row, List } from "antd";
import { DeleteFilled, FolderAddFilled } from "@ant-design/icons";
import Tags from "../Tags";
import EditableTitle from "./EditableTitle";

const DeleteLink = (link, links, setLinks) => {
  console.log(setLinks);
  return (
    <Row>
      <a
        onClick={() =>
          fetch("http://127.0.0.1:8088/deletelink", {
            method: "POST",
            body: JSON.stringify(link),
            headers: {
              "Content-Type": "application/json",
            },
          }).then((res) => {
            var filteredLinks = links.filter((currentLink) => {
              if (currentLink.id !== link.id) return currentLink;
            });
            console.log(setLinks);
            setLinks({ links: filteredLinks });
            return res;
          })
        }
      >
        <DeleteFilled style={{ paddingRight: "5px", fontSize: "20px" }} />
      </a>
    </Row>
  );
};

function Links() {
  const [links, setLinks] = FetchLinks();

            console.log(setLinks)
  return (
    <List
      itemLayout="vertical"
      size="small"
      dataSource={links}
      renderItem={(link) => (
        <List.Item key={link.id}>
          <List.Item.Meta title={<EditableTitle link={link} />} />
          <List.Item.Meta description={DeleteLink(link, links, setLinks)} />
        </List.Item>
      )}
    />
  );
}
//     if (this.state.status == "isLoaded") {
//       return (
//         <List
//           itemLayout="vertical"
//           size="large"
//           dataSource={this.state.links}
//           renderItem={(item) => (
//             <List.Item key={item.id} actions={[]}>
//               <List.Item.Meta title={<EditableTitle link={item} />} />
//               <List.Item.Meta
//                 description={
//                   <Row>
//                     <a href="#" onClick={() => this.deleteLink(item)}>
//                       <DeleteFilled
//                         style={{ paddingRight: "5px", fontSize: "20px" }}
//                       />
//                     </a>
//                     <FolderAddFilled
//                       style={{ paddingRight: "25px", fontSize: "20px" }}
//                     />
//                     <Tags/>
//                   </Row>
//                 }
//               />
//             </List.Item>
//           )}
//         />
//       );
//     } else {
//       return <div>Loading</div>;
//     }
//   }
// )

// class Links extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       status: "notLoaded",
//       links: {},
//     };

//     this.deleteLink = this.deleteLink.bind(this);
//     this.linkManagement = this.linkManagement.bind(this);
//   }

//   componentDidMount() {
//     this.setState({ loading: "isLoading" });
//     fetch("http://127.0.0.1:8088/getlinks")
//       .then((response) => response.json())
//       .then((data) => {
//         data.sort((a, b) => a.id - b.id);
//         this.setState({
//           status: "isLoaded",
//           links: data,
//         });
//       });
//   }

//   deleteLink = (linkToDelete) => {
//     fetch("http://127.0.0.1:8088/deletelink", {
//       method: "POST",
//       body: JSON.stringify(linkToDelete),
//       headers: {
//         "Content-Type": "application/json",
//       },
//     })
//       .then((res) => {
//         var filteredLinks = this.state.links.filter((link) => {
//           if (link.id !== linkToDelete.id) {
//             return link;
//           }
//         });

//         this.setState({
//           links: filteredLinks,
//         });
//         return res;
//       })
//       .catch((err) => console.log(err));
//   };

//   linkManagement = (item) => {
//     return (
//       <Row>
//         <a href="#" onClick={() => this.deleteLink(item)}>
//           <DeleteFilled style={{ paddingRight: "5px", fontSize: "20px" }} />
//         </a>
//         <FolderAddFilled style={{ paddingRight: "25px", fontSize: "20px" }} />
//       </Row>
//     );
//   }

//   render() {
//     if (this.state.status == "isLoaded") {
//       return (
//         <List
//           itemLayout="vertical"
//           size="large"
//           dataSource={this.state.links}
//           renderItem={(item) => (
//             <List.Item key={item.id} actions={[]}>
//               <List.Item.Meta title={<EditableTitle link={item} />} />
//               <List.Item.Meta
//                 description={
//                   <Row>
//                     <a href="#" onClick={() => this.deleteLink(item)}>
//                       <DeleteFilled
//                         style={{ paddingRight: "5px", fontSize: "20px" }}
//                       />
//                     </a>
//                     <FolderAddFilled
//                       style={{ paddingRight: "25px", fontSize: "20px" }}
//                     />
//                     <Tags/>
//                   </Row>
//                 }
//               />
//             </List.Item>
//           )}
//         />
//       );
//     } else {
//       return <div>Loading</div>;
//     }
//   }
// }
export default Links;
