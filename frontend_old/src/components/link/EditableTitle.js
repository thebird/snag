import React from "react"
import { EditOutlined } from '@ant-design/icons'
import { Input } from 'antd';

class EditableTitle extends React.Component {

  constructor(props) {
    super();
    this.state = {
      link: props.link,
      editing: false,
    };

    this.handleToggleEditing = this.handleToggleEditing.bind(this);
    this.handleUpdateTitle = this.handleUpdateTitle.bind(this);
  }

  handleToggleEditing() {
   if (this.state.editing) {
      this.setState({ editing: false })
    } else {
      this.setState({ editing: true })
    }
  }

  handleUpdateTitle = (e) => {
    var old_value = this.state.link.title;
    this.state.link.title = e.target.value;

    fetch("http://127.0.0.1:8088/updatelink", {
      method: "POST",
      body: JSON.stringify(this.state.link),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        this.setState({ link: this.state.link, editing: false })
        return res;
      })
      .catch((err) => {
        this.state.link.title = old_value
        console.log(err)
      })
  };

  render() {
    var title = (
      <a href={this.state.link.link}>
        {this.state.link.title.slice(0, 80) + "..."}
      </a>
    );
    if (this.state.editing == true) {
      return (
        <div>
          <EditOutlined
            onClick={this.handleToggleEditing}
            style={{ paddingRight: "10px" }}
          />

          <Input
            defaultValue={this.state.link.title}
            onPressEnter={this.handleUpdateTitle}
            style={{ width: "80%" }}
          />
        </div>
      );
    }
    return (
      <div>
        <EditOutlined
          onClick={this.handleToggleEditing}
          style={{ paddingRight: "10px" }}
        />
        {title}
      </div>
    );
  }
}

export default EditableTitle
