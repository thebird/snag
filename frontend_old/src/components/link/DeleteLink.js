import React from "react";
import { Row } from "antd";
import { DeleteFilled } from "@ant-design/icons";


const DeleteLink = (link, setLinks) => {
  return (
    <Row>
      <a
        href="#"
        onClick={() =>
          fetch("http://127.0.0.1:8088/deletelink", {
            method: "POST",
            body: JSON.stringify(linkToDelete),
            headers: {
              "Content-Type": "application/json",
            },
          }).then((res) => {
            var filteredLinks = this.state.links.filter((currentLink) => {
              if (currentLink.id !== link.id) {
                return currentLink;
              }
            });
            setLinks(filteredLinks);
            return res;
          })
        }
      >
        <DeleteFilled style={{ paddingRight: "5px", fontSize: "20px" }} />
      </a>
    </Row>
  );
};

export default DeleteLink;
