import { useState, useEffect } from "react";

function FetchLinks() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  async function fetchLinks() {
    const response = await fetch("http://127.0.0.1:8088/getlinks");
    const json = await response.json();

    json.sort((a, b) => a.id - b.id)
    setData(json);
    setLoading(false);
  }
  
  useEffect(() => {
    fetchLinks();
  }, []);
  return [data, loading];
}

export { FetchLinks };
