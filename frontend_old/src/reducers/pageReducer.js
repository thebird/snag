export const initialState = {
  page: "links",
}

const pageReducer = (state = initialState, action) => {
  console.log(state)
  switch (action.type) {
    case "PAGE_CHANGE":
      return {
        page: action.page,
      }
    default:
      return state.page
  }
}

export default pageReducer
