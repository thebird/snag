import React from "react";
import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Layout } from "antd";
import Sidebar from "./components/Sidebar";

import "/css/custom.css";

const { Content } = Layout;

function App() {
  const page = useSelector((state) => state.page);

  return (
    // <Router>
      <Sidebar />
    //   <Layout style={{ minHeight: "100vh" }}>
    //     <Layout className="site-layout">{ page }</Layout>
    //   </Layout>
    // </Router>
  )
}

// class App extends React.Component {

//     constructor() {
//         super()
//         this.state = {
//             page: 1
//         }
//         this.handleMenuClick = this.handleMenuClick.bind(this)
//     }

//     handleMenuClick(menu) {
//         this.setState({
//             page: menu.key
//         })
//     };

//     render() {
//         var page = <Links/>
//         if(this.state.page == 1) {
//             page = <Links/>
//         } else if (this.state.page == 2) {
//             page = <Quotes/>
//         } else {
//             page = <Images/>
//         }

//         return (
//             <Router>
//             <Layout style={{ minHeight: '100vh' }}>
//                 <Sidebar handleClick={this.handleMenuClick} />
//                 <Layout className='site-layout'>

//                 <Content>
//                     {page}
//                 </Content>
//                 </Layout>
//             </Layout>
//             </Router>
//         );
//     }
// }

export default App;
