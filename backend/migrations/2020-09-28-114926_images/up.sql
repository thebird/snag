DROP TABLE IF EXISTS images;

CREATE TABLE images (
    id SERIAL NOT NULL PRIMARY KEY,
    link TEXT NOT NULL,
    image_path TEXT NOT NULL,
    created TIMESTAMP NOT NULL
);