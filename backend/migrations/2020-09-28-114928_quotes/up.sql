DROP TABLE IF EXISTS quotes;

CREATE TABLE quotes (
    id SERIAL NOT NULL PRIMARY KEY,
    context_link TEXT NOT NULL,
    context_title TEXT NOT NULL,
    quote TEXT NOT NULL,
    created TIMESTAMP NOT NULL
)