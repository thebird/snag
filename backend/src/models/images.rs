use crate::schema::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Queryable, PartialEq)]
pub struct Image {
    pub id: i32,
    pub link: String,
    pub image_path: String,
    pub created: chrono::NaiveDateTime,
}

#[derive(Insertable, Debug)]
#[table_name = "images"]
pub struct ImageNew<'a> {
    pub link: &'a str,
    pub image_path: &'a str,
    pub created: chrono::NaiveDateTime,
}

#[derive(Insertable, Debug, AsChangeset)]
#[table_name = "images"]
pub struct ImageUpdate<'a> {
    pub id: &'a i32,
    pub link: &'a str,
    pub image_path: &'a str,
    pub created: chrono::NaiveDateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ImageJson {
    pub link: String,
}


