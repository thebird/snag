use crate::schema::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Queryable, PartialEq)]
pub struct Quote {
    pub id: i32,
    pub context_link: String,
    pub context_title: String,
    pub quote: String,
    pub created: chrono::NaiveDateTime,
}

#[derive(Insertable, Debug)]
#[table_name = "quotes"]
pub struct QuoteNew<'a> {
    pub context_link: &'a str,
    pub context_title: &'a str,
    pub quote: &'a str,
    pub created: chrono::NaiveDateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct QuoteJson {
    pub context_link: String,
    pub context_title: String,
    pub quote: String,
}
