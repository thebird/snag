use crate::schema::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Queryable, PartialEq)]
pub struct Link {
    pub id: i32,
    pub link: String,
    pub title: String,
    pub created: chrono::NaiveDateTime,
}

#[derive(Insertable, Debug)]
#[table_name = "links"]
pub struct LinkNew<'a> {
    pub link: &'a str,
    pub title: &'a str,
    pub created: chrono::NaiveDateTime,
}

#[derive(Insertable, Debug, AsChangeset)]
#[table_name = "links"]
pub struct LinkUpdate<'a> {
    pub id: &'a i32,
    pub link: &'a str,
    pub title: &'a str,
    pub created: chrono::NaiveDateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LinkJson {
    pub link: String,
}
