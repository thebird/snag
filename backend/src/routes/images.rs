use crate::models::images::{Image, ImageJson, ImageNew, ImageUpdate};
use crate::Pool;
use actix_web::{web, Error, HttpResponse};
use diesel::dsl::insert_into;
use std::path::Path;
use anyhow::{Result};
use tokio::fs::File;
use tokio::io::{AsyncWriteExt};
use diesel::prelude::*;

/* 
 * Things to fix:
 * - Error handling on add_image to handle when the image isn't downloaded or not found. 
 * - Comment all procedures.
 * - Pull domain from download_image 
*/

pub async fn add_image(
    pool: web::Data<Pool>,
    item: web::Json<ImageJson>,
) -> Result<HttpResponse, Error> {
    let image_path = download_image(&item.link).await.unwrap();
    Ok(web::block(move || add_single_image(pool, item, image_path))
        .await
        .map(|link| HttpResponse::Created().json(link))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

async fn download_image(url: &String) -> Result<String, Box<dyn std::error::Error>> {
    std::env::set_var("RUST_LOG", "actix_web=debug");
    println!("here");
    let response = reqwest::get(url).await?;
    let fname = response
                .url()
                .path_segments()
                .and_then(|segments| segments.last())
                .and_then(|name| if name.is_empty() { None } else { Some(name) })
                .unwrap_or("tmp.bin");
    let p = Path::new("files/images/");
    let p = p.join(fname);
    println!("{:?}", p);
    let mut file = File::create(&p).await?;
    let content =  response.bytes().await?;
    file.write_all(&content).await?;
    Ok(p.as_path().display().to_string())
}

fn add_single_image(
    pool: web::Data<Pool>,
    item: web::Json<ImageJson>,
    img_path: String
) -> Result<Image, diesel::result::Error> {
    use crate::schema::images::dsl::*;
    let conn = pool.get().unwrap();

    match images.filter(link.eq(&item.link)).first::<Image>(&conn) {
        Ok(res) => Ok(res),
        Err(_) => {
            let new_image = ImageNew {
                link: &item.link,
                image_path: img_path.as_str(),
                created: chrono::Local::now().naive_local(),
            };
            let res = insert_into(images).values(&new_image).get_result(&conn)?;
            Ok(res)
        }
    }
}

pub async fn get_images(pool: web::Data<Pool>) -> Result<HttpResponse, Error> {
    Ok(web::block(move || get_all_images(pool))
        .await
        .map(|images| HttpResponse::Ok().json(images))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn get_all_images(pool: web::Data<Pool>) -> Result<Vec<Image>, diesel::result::Error> {
    use crate::schema::images::dsl::*;
    let conn = pool.get().unwrap();
    let result = images.load::<Image>(&conn)?;
    Ok(result)
}

