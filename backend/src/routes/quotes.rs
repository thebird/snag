use crate::models::quotes::{Quote, QuoteJson, QuoteNew};
use crate::Pool;
use actix_web::{web, Error, HttpResponse};
use diesel::dsl::insert_into;
use diesel::RunQueryDsl;

pub async fn add_quote(
    db: web::Data<Pool>,
    item: web::Json<QuoteJson>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || add_single_quote(db, item))
        .await
        .map(|quote| HttpResponse::Created().json(quote))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn add_single_quote(
    db: web::Data<Pool>,
    item: web::Json<QuoteJson>,
) -> Result<Quote, diesel::result::Error> {
    use crate::schema::quotes::dsl::*;
    let conn = db.get().unwrap();
    let new_quote = QuoteNew {
        context_link: &item.context_link,
        context_title: &item.context_title,
        quote: &item.quote,
        created: chrono::Local::now().naive_local(),
    };

    let res = insert_into(quotes).values(&new_quote).get_result(&conn)?;
    Ok(res)
}

pub async fn get_quotes(db: web::Data<Pool>) -> Result<HttpResponse, Error> {
    Ok(web::block(move || get_all_quotes(db))
        .await
        .map(|links| HttpResponse::Ok().json(links))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn get_all_quotes(pool: web::Data<Pool>) -> Result<Vec<Quote>, diesel::result::Error> {
    use crate::schema::quotes::dsl::*;
    let conn = pool.get().unwrap();
    let result = quotes.load::<Quote>(&conn)?;
    Ok(result)
}
