use crate::models::links::{Link, LinkJson, LinkNew, LinkUpdate};
use crate::Pool;
use actix_web::{web, Error, HttpResponse};
use diesel::dsl::insert_into;
use diesel::prelude::*;
use regex::Regex;
use reqwest::Url;


pub async fn add_link(
    pool: web::Data<Pool>,
    item: web::Json<LinkJson>,
) -> Result<HttpResponse, Error> {
    let title = get_site_title(&item.link).await.unwrap();
    Ok(web::block(move || add_single_link(pool, item, title))
        .await
        .map(|link| HttpResponse::Created().json(link))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

async fn get_site_title(url: &String) -> Result<String, Box<dyn std::error::Error>> {
    let body = reqwest::get(Url::parse(&url).unwrap())
        .await?
        .text()
        .await?;
    let re = Regex::new("<title>(.*?)</title>").unwrap();
    match re.captures(body.as_str()) {
        Some(data) => Ok(data.get(1).unwrap().as_str().to_owned()),
        None => Ok("None".to_string()),
    }
}

fn add_single_link(
    pool: web::Data<Pool>,
    item: web::Json<LinkJson>,
    site_title: String,
) -> Result<Link, diesel::result::Error> {
    use crate::schema::links::dsl::*;
    let conn = pool.get().unwrap();

    match links.filter(link.eq(&item.link)).first::<Link>(&conn) {
        Ok(res) => Ok(res),
        Err(_) => {
            let new_link = LinkNew {
                link: &item.link,
                title: &site_title,
                created: chrono::Local::now().naive_local(),
            };
            let res = insert_into(links).values(&new_link).get_result(&conn)?;
            Ok(res)
        }
    }
}

pub async fn get_links(pool: web::Data<Pool>) -> Result<HttpResponse, Error> {
    Ok(web::block(move || get_all_links(pool))
        .await
        .map(|links| HttpResponse::Ok().json(links))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn get_all_links(pool: web::Data<Pool>) -> Result<Vec<Link>, diesel::result::Error> {
    use crate::schema::links::dsl::*;
    let conn = pool.get().unwrap();
    let result = links.load::<Link>(&conn)?;
    Ok(result)
}


pub async fn update_link(
    pool: web::Data<Pool>,
    item: web::Json<Link>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || update_single_link(pool, item))
        .await
        .map(|updated_id| HttpResponse::Created().json(updated_id))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn update_single_link(pool: web::Data<Pool>, item: web::Json<Link>) -> QueryResult<usize> {
    use crate::schema::links::dsl::*;
    let updated_link = LinkUpdate {
        id:&item.id,
        link:&item.link,
        title:&item.title,
        created: item.created
    };
    let conn = pool.get().unwrap();
    diesel::update(links.find(updated_link.id)).set(updated_link).execute(&conn)
}

pub async fn delete_link(
    pool: web::Data<Pool>,
    item: web::Json<Link>
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || delete_single_link(pool, item))
        .await
        .map(|deleted_id| HttpResponse::Created().json(deleted_id))
        .map_err(|_| HttpResponse::InternalServerError())?)
}

fn delete_single_link(
    pool: web::Data<Pool>,
    item: web::Json<Link>
) -> QueryResult<usize>{
    use crate::schema::links::dsl::*;
    let conn = pool.get().unwrap();
    diesel::delete(links.filter(id.eq(item.id))).execute(&conn)
}