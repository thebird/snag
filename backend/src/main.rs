#[macro_use]
extern crate diesel;

mod models;
mod routes;
mod schema;

use actix_cors::Cors;
use actix_web::{http::header, web, App, HttpServer};
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

use actix_files::NamedFile;
use actix_web::{HttpRequest, Result};
use std::path::PathBuf;
use actix_files as fs;

async fn getfile(req: HttpRequest) -> Result<NamedFile> {
    std::env::set_var("RUST_LOG", "actix_web=debug");
    let path: PathBuf = req.match_info().query("filename").parse().unwrap();
    println!("{:?}", path);  
    Ok(NamedFile::open(path)?)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    std::env::set_var("RUST_LOG", "actix_web=debug");
    println!("Starting server...");
    let database_url = std::env::var("DATABASE_URL").expect("DB url not found");

    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool: Pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool");

    HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .wrap(
                Cors::new()
                    .allowed_methods(vec!["GET", "POST"])
                    .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                    .allowed_header(header::CONTENT_TYPE)
                    .max_age(3600)
                    .finish(),
            )
            .service(web::resource("/getlinks").route(web::get().to(routes::links::get_links)))
            .service(web::resource("/deletelink").route(web::post().to(routes::links::delete_link)))
            .service(web::resource("/addlink").route(web::post().to(routes::links::add_link)))
            .service(web::resource("/updatelink").route(web::post().to(routes::links::update_link)))
            .service(web::resource("/addimage").route(web::post().to(routes::images::add_image)))
            .service(web::resource("/getimages").route(web::get().to(routes::images::get_images)))
            .service(fs::Files::new("/", ".").show_files_listing())
            .service(web::resource("/files/images/{filename:.*}").route(web::get().to(getfile)))
        // .route("/addquote", web::post().to(routes::quotes::add_quote))
        // .route("/getquotes", web::post().to(routes::quotes::get_quotes))
    })
    .bind("127.0.0.1:8088")?
    .run()
    .await

}
