table! {
    images (id) {
        id -> Int4,
        link -> Text,
        image_path -> Text,
        created -> Timestamp,
    }
}

table! {
    links (id) {
        id -> Int4,
        link -> Text,
        title -> Text,
        created -> Timestamp,
    }
}

table! {
    quotes (id) {
        id -> Int4,
        context_link -> Text,
        context_title -> Text,
        quote -> Text,
        created -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(
    images,
    links,
    quotes,
);
